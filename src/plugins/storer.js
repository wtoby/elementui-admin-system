import Vue from 'vue'
import Vuex from 'vuex'
// 加载模块
import base from '@/stores/base'

Vue.use(Vuex)

const state = {}
const getters = {}
const mutations = {}
const actions = {}
const modules = {}

// 共享数据
state.sex = '女'

// 包装

// 设置

// 异步

// 模块
modules.base = base

export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters,
    modules
})
