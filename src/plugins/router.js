import Vue from 'vue'
import VueRouter from 'vue-router'

// 路由模块
import base from '@/routes/base'

Vue.use(VueRouter)

const router = new VueRouter({
    routes: [...base]
})

export default router
