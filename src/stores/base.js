const state = {}
const getters = {}
const mutations = {}
const actions = {}

// 共享数据
state.user = 10

// 包装

// 设置
mutations.add = (state, params) => {
    state.user += params
    return state.user
}

// 异步

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
