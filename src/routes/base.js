export default [
    {
        path: '/',
        name: 'Index',
        component: () => import('@/views/base/Index.vue')
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('@/views/base/Login.vue')
    }
]
