module.exports = {
    devServer: {
        proxy: {
            '/api/': {
                target: 'http://www.baidu.com/api/login',
                ws: false,
                changeOrigin: true
            }
        },
        open: true
    }
}
